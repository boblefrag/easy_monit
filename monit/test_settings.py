import os

from monit.base_settings import *

if os.environ.get("CI") == "gitlabci":
    DATABASES['default'].update(
        {'HOST': os.environ["DATABASE_HOST"],
         'USER': os.environ["POSTGRES_USER"]}
    )
